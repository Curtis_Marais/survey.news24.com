﻿using Microsoft.AspNetCore.Mvc;

namespace survey.news24.com.Controllers
{
    public class HomeController : Controller
    {
        public IActionResult Index()
        {
            return View();
        }
    }
}