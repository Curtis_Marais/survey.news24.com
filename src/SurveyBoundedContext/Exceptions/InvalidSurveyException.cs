﻿using System;

namespace SurveyBoundedContext.Exceptions
{
    public class InvalidSurveyException : Exception
    {
        public InvalidSurveyException(string message) : base(message)
        {
        }
    }

    public class SurveyExistsException : Exception
    {
        public SurveyExistsException(string message) : base(message)
        {
        }
    }
}