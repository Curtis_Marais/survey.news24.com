﻿using System;
using System.Collections.Generic;
using SurveyBoundedContext.SurveyAggregate;
using UserBoundedContext.UserAggregate;

namespace SurveyBoundedContext.CampaignAggregate
{
    public class Campaign
    {
        public Guid Id { get; set; }
        public Survey Survey { get; set; }
        public IEnumerable<User> Entries { get; set; }
    }
}