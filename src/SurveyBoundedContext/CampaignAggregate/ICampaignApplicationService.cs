﻿namespace SurveyBoundedContext.CampaignAggregate
{
    public interface ICampaignApplicationService
    {
        void AddCampaign();
        void RemoveCampaign();
        void UpdateCampaign();
    }
}