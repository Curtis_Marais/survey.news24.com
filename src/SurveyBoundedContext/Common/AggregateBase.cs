﻿using System;

namespace SurveyBoundedContext.Common
{
    public abstract class AggregateBase
    {
        public Guid Id { get; set; }
        internal abstract void Validate();
    }
}