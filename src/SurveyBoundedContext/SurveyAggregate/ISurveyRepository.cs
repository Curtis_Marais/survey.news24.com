﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;

namespace SurveyBoundedContext.SurveyAggregate
{
    public interface ISurveyRepository
    {
        Survey GetSurvey(Guid surveyId);
        IEnumerable<Survey> GetSurveys(Expression<Func<Survey, bool>> predicate);
        bool SurveyExists(Survey survey);
        bool SurveyExists(Guid surveyId);
        void CreateSurvey(Survey survey);
        void CreateSurveys(IEnumerable<Survey> surveys);
        void RemoveSurvey(Guid surveyId);
        void RemoveSurvey(Survey survey);
        void UpdateSurvey(Survey survey);
        void UpdateSurveys(IEnumerable<Survey> surveys);
    }
}