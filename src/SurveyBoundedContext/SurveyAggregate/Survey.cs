﻿using System;
using System.Collections.Generic;
using SurveyBoundedContext.Common;
using SurveyBoundedContext.Exceptions;

namespace SurveyBoundedContext.SurveyAggregate
{
    public class Survey : AggregateBase
    {
        public string Name { get; set; }
        public DateTime CreatedAt { get; set; }
        public DateTime UpdatedAt { get; set; }
        public DateTime? OpeningDate { get; set; }
        public DateTime? ClosingDate { get; set; }
        public IEnumerable<Question> Questions { get; set; }

        internal override void Validate()
        {
            if (Id == Guid.Empty)
                throw new InvalidSurveyException($"{nameof(Id)} cannot be empty guid");
            if (string.IsNullOrEmpty(Name))
                throw new InvalidSurveyException($"{nameof(Name)} cannot be empty");
            if (!OpeningDate.HasValue)
                throw new InvalidSurveyException($"{nameof(OpeningDate)} cannot be null");
            if (!ClosingDate.HasValue)
                throw new InvalidSurveyException($"{nameof(ClosingDate)} cannot be null");
        }
    }
}