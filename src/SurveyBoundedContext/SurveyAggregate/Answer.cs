﻿using System;

namespace SurveyBoundedContext.SurveyAggregate
{
    public class Answer
    {
        public Guid Id { get; set; }
        public string Text { get; set; }
    }
}