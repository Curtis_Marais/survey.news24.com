﻿using System;
using System.Collections.Generic;

namespace SurveyBoundedContext.SurveyAggregate
{
    public abstract class Question
    {
        public Guid Id { get; set; }
        public string Text { get; set; }
    }

    public class MultipleSelectQuestion : Question
    {
        public IEnumerable<Answer> Answers { get; set; }
    }

    public class SelectQuestion : Question
    {
        public Answer Answer { get; set; }
    }
}