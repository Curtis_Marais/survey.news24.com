﻿using SurveyBoundedContext.Exceptions;
using SurveyBoundedContext.SurveyAggregate;

namespace SurveyBoundedContext
{
    public class SurveyApplicationService : ISurveyApplicationService
    {
        public SurveyApplicationService(ISurveyRepository surveyRepository)
        {
            SurveyRepository = surveyRepository;
        }

        private ISurveyRepository SurveyRepository { get; }

        public void CreateSurvey(Survey survey)
        {
            survey.Validate();
            if (SurveyRepository.SurveyExists(survey.Id))
                throw new SurveyExistsException($"Survey {survey.Name} already exists");
            SurveyRepository.CreateSurvey(survey);
        }
    }
}