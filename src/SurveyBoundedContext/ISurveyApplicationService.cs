﻿using SurveyBoundedContext.SurveyAggregate;

namespace SurveyBoundedContext
{
    public interface ISurveyApplicationService
    {
        void CreateSurvey(Survey survey);
    }
}