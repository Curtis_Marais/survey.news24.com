﻿using System;

namespace UserBoundedContext.UserAggregate
{
    public class User
    {
        private Guid Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public int Phone { get; set; }
        public string Email { get; set; }
        public Gender Gender { get; set; }
        public DateTime DateOfBirth { get; set; }
        public bool EmailConfirmed { get; set; }
        public DateTime CreatedAt { get; set; }
        public DateTime UpdatedAt { get; set; }
        public string Qualification { get; set; }
        public Race Race { get; set; }
    }

    public enum Race
    {
        African,
        Coloured,
        Indian,
        White,
        Other
    }

    public enum Gender
    {
        Male,
        Female
    }
}