﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;

namespace UserBoundedContext.UserAggregate
{
    public interface IUserRepository
    {
        void CreateUser(User user);
        void RemoveUser(User user);
        void RemoveUser(Guid userId);
        void UpdateUser(User user);
        User GetUser(Guid userId);
        IEnumerable<User> GetAllUsers(Expression<Func<User, bool>> predicate = null);
    }
}