﻿using System;
using Moq;
using NUnit.Framework;
using SurveyBoundedContext;
using SurveyBoundedContext.Exceptions;
using SurveyBoundedContext.SurveyAggregate;

namespace SurveyBoundedContext.UnitTests.Survey
{
    [TestFixture]
    public class SurveyApplicationTests
    {
        [SetUp]
        public void Setup()
        {
            surveyRepositoryProxy = new Mock<ISurveyRepository>();
            applicaitonService = new SurveyApplicationService(surveyRepositoryProxy.Object);
            validSurvey = new SurveyBoundedContext.SurveyAggregate.Survey
            {
                Id = Guid.NewGuid(),
                Name = "Valid survey",
                OpeningDate = DateTime.Now,
                ClosingDate = DateTime.Now.AddDays(52)
            };
            invalidSurvey = new SurveyBoundedContext.SurveyAggregate.Survey();
        }

        private Mock<ISurveyRepository> surveyRepositoryProxy;
        private SurveyApplicationService applicaitonService;
        private SurveyAggregate.Survey validSurvey;
        private SurveyAggregate.Survey invalidSurvey;

        [Test]
        public void CreateSurvey_WithExistingSurvey_ThrowsSurveyExistsException()
        {
            surveyRepositoryProxy.Setup(sr => sr.SurveyExists(validSurvey.Id)).Returns(true);
            Assert.Throws<SurveyExistsException>(() => applicaitonService.CreateSurvey(validSurvey));
        }

        [Test]
        public void CreateSurvey_WithInvalidSurvey_ThrowsInvalidSurveyException()
        {
            Assert.Throws<InvalidSurveyException>(() => applicaitonService.CreateSurvey(invalidSurvey));
        }

        [Test]
        public void CreateSurvey_WithSurvey_InvokesSurveyRepositoryCreateSurvey()
        {
            var wasInvoked = false;
            surveyRepositoryProxy
                .Setup(sr => sr.CreateSurvey(validSurvey))
                .Callback(() => wasInvoked = true);

            applicaitonService.CreateSurvey(validSurvey);
            Assert.True(wasInvoked);
        }
    }
}