using Moq;
using NUnit.Framework;
using SurveyBoundedContext.CampaignAggregate;

namespace SurveyBoundedContext.UnitTests.CampaignAggregate
{
    [TestFixture]
    public class CampaignApplicationServiceTests
    {
        [SetUp]
        public void TestInitialize()
        {
            mockRepository = new MockRepository(MockBehavior.Strict);
        }

        [TearDown]
        public void TestCleanup()
        {
            mockRepository.VerifyAll();
        }

        private MockRepository mockRepository;

        private CampaignApplicationService CreateService()
        {
            return new CampaignApplicationService();
        }

        [Test]
        public void TestMethod1()
        {
            CampaignApplicationService service = CreateService();
        }
    }
}